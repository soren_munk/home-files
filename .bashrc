
# If not running interactively, don't do anything
[ -z "$PS1" ] && return

if [ "$SSH_TTY" ]; then
   #keychain -q ~/.ssh/id_dsa ~/.ssh/id_rsa.vps147
   #. ~/.keychain/$HOSTNAME-sh
   #. ~/.keychain/$HOSTNAME-sh-gpg
   false
fi

if [ "$SSH_TTY" ]; then
   echo ""
   sleep 0.1
   timeout -s 9 1s grep "cpu MHz" /proc/cpuinfo
   echo ""
   timeout -s 9 1s df --total -x rootfs -x tmpfs -x devtmpfs --block-size=1G;
fi

PATH=$PATH:$HOME/bin
PATH=$PATH:$HOME/bitbucket/root-scripts
export PATH

if [ ! "$DISPLAY" ]; then
  export DISPLAY=localhost:0.0
fi

complete -W "$(echo $(grep '^ssh ' ~/.bash_history | sort -u | sed 's/^ssh //'))" ssh

HISTFILESIZE="10000"
HISTSIZE="INFINITE"

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

